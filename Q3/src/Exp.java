import java.util.ArrayList;

/*
 * Exp 
 * Eric McCreath 2017
 */

public abstract class Exp {
 
	public abstract int evaluate(Subs subs, Functions funs);

	public abstract String show();
	static boolean loop_out = false;
	static boolean flag = false;
	static Exp test , exp1, exp2;
	//constructor 
	
	static public Exp parseExp(Tokenizer tok) {
		 
		while(tok.hasNext()) {
			if(tok.current().equals("(") && tok.previous_token()== null) {
				System.out.println(" i am in '(' "+ tok.current());
				tok.next();
				
				while(!tok.current().equals("?")) {
					System.out.println(" i am before '?' " + tok.current());
					 test  = parseExp(tok);
					 removebracket(tok);
				}
				if(tok.current().equals("?"))
					System.out.println("I am in '?'");
					tok.next();
				
					while(!tok.current().equals(":") ) {
						System.out.println("i am in ':'");
						while( tok.current().equals(")") ) {
							tok.next();
							flag = true;
							}
						if(flag == false) {
							 exp1  = parseExp(tok);
						}
					}
				tok.next();
			 exp2 = parseExp(tok);
			return new SelectExp(test,exp1,exp2);
				
			}
				
			
		 
		 if(tok.pos < tok.text.length()   &&tok.current().equals(0)) 
				{
			 System.out.println(" i am in '0' ");
				tok.next();
				return new ZeroExp();
				}
			 if(tok.pos < tok.text.length()   &&tok.current().equals("inc")) {
				 System.out.println(" i am in 'inc' ");
				tok.next();
				tok.next();
					return new IncExp(parseExp(tok));
			}
			
			 if(tok.pos < tok.text.length()   &&tok.current().equals("dec")) {
				 System.out.println(" i am in 'dec' ");
				tok.next();
				tok.next();
				return new DecExp(parseExp(tok));	
			}
			
		
		}
		return null;
	}

	private static ArrayList<Exp> parseExps(Tokenizer tok) {
		// add your code here
		return null;
	}

	private static Function parseFunction(Tokenizer tok) {
		
		// add your code here
		return null;
	}

	private static Vars parseVars(Tokenizer tok) {
		// add your code here
		return null;
	}
	
	
	public static Functions parseFunctions(Tokenizer tok) {
		// add your code here
		return null;
	}
	
	public static void removebracket(Tokenizer tok) {
		if(tok.current()==")") tok.next();
	}

}
